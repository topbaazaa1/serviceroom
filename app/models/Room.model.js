const sql = require("./db");

const Room = function(room) {
    this.RoomID = room.RoomID;
    this.Roomname = room.Roomname;
    this.Build = room.Build;
    this.Capacity = room.Capacity;
  };
  
  module.exports = Room;
  