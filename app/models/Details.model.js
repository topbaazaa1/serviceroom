const sql = require("./db");


const Details = function (details) {
    this.DetailId = details.DetailId;
    this.TimeStart = details.TimeStart;
    this.TimeEnd = details.TimeEnd;
    this.Dates  = details.Dates;
    this.RoomID = details.RoomID;
    this.BookingId = details.BookingId
    this.Status = details.Status
    this.Build = details.Build
    this.Capacity = details.Capacity
    this.Roomname = details.Roomname
    this.Subject = details.Subject
    
};
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
  
Details.search = (searchDetail, result) => {
  sql.query(`SELECT Room.Roomname , Room.Build , Timeslot.TimeStart , Timeslot.TimeEnd ,Details.Dates  
   FROM projectroom.Details ,projectroom.Timeslot ,projectroom.Room 
  WHERE  Details.TimeId = Timeslot.TimeId  and Details.RoomID = Room.RoomID  
  and BookingId is  null and TimeStart BETWEEN ? 
  AND ADDTIME(?,'00:-30:00') AND  TimeEnd BETWEEN ADDTIME(?,'00:30:00') AND ? 
  And Status = ? And  Room.Build= ? 
  And  Room.Capacity = ? 
  And  Details.Dates = ?;`, [searchDetail.TimeStart,searchDetail.TimeEnd,searchDetail.TimeStart,searchDetail.TimeEnd,searchDetail.Status,searchDetail.Build,searchDetail.Capacity,searchDetail.Dates], 
  (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log(" show", res);
    result(null, res);

  });
}

Details.update_booking = (updateDetails_booking, result) => {
  
  sql.query(`UPDATE projectroom.Details SET BookingId = ?, status = 'W' 
  WHERE RoomID = ?
  and dates = ?
  and TimeId = (Select TimeId from projectroom.Timeslot where TimeStart = ?);`,
  [updateDetails_booking.BookingId, updateDetails_booking.RoomID,updateDetails_booking.Dates,updateDetails_booking.TimeStart], 
  (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    return;
    }
    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("update Success: ", updateDetails_booking);
    result(null, res);
    
  });
  // sleep(3000);
};

Details.delete = (deleteDetails, result) => {
  sql.query("DELETE FROM `projectroom`.`Booking` WHERE `BookingId` = ?", deleteDetails.BookingId, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    return;
    }
    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Success: ", deleteDetails);
    result(null, res);
  });
  // sql.query("UPDATE `projectroom`.`Details` SET `status` = 'A'  WHERE `DetailId`  = ?", updateDetails.DetailId, (err, res) => {
  //   if (err) {
  //     console.log("error: ", err);
  //     result(null, err);
  //   return;
  //   }
  //   if (res.affectedRows == 0) {
  //     // not found Customer with the id
  //     result({ kind: "not_found" }, null);
  //     return;
  //   }

  //   console.log("update Success: ", updateDetails);
  //   result(null, res);
  // });
};

Details.update = (updateDetails, result) => {
  sql.query("UPDATE `projectroom`.`Details` SET BookingId = null,`status` = 'A'  WHERE `BookingId`  = ?", updateDetails.BookingId, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    return;
    }
    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("update Success: ", updateDetails);
    result(null, res);
  });
};


module.exports = Details;
