const sql = require("./db");

// constructor
const Booking = function(booking) {
  this.BookingId = booking.BookingId;
  this.Subjects = booking.Subjects;
  this.Dates = booking.Dates;
  this.TID = booking.TID
  this.TimeStart = booking.TimeStart;
  this.TimeEnd = booking.TimeEnd;
  this.Dates  = booking.Dates;
  this.RoomID = booking.RoomID;
};

Booking.create = (newBooking, result) => {
  sql.query(`INSERT INTO projectroom.Booking (Dates, Subjects, TID) VALUES (now(), ?, ?);`, [newBooking.Subjects,newBooking.TID], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    // console.log("created Details: ", { BookingId: res.insertId, ...newBooking });
    result(null, { id: res.insertId, ...newBooking });
  });
};

Booking.delete = (deleteBooking, result) => {
  sql.query("DELETE FROM `projectroom`.`Booking` WHERE `BookingId` = ?", deleteBooking.BookingId, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted Success: ", deleteBooking);
    result(null, res);
  });
};

// Booking.create = (newBooking, result) => {
//   sql.query(`INSERT INTO projectroom.Booking (Dates, Subjects, TID) VALUES (?, ?, ?);`, [newBooking.Dates,newBooking.Subject,newBooking.TID], (err, res) => {
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//       return;
//     }

//     console.log("created Details: ", { id: res.insertId, ...newBooking });
//     result(null, { id: res.insertId, ...newBooking });
//   });
// };
module.exports = Booking;
